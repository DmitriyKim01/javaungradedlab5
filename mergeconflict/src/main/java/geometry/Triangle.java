package geometry;
public class Triangle{
    private int base;
    private int height;
    public Triangle(int base, int height){
        this.base = base;
        this.height = height;
    }
    public int getBase(){
        return this.base;
    }
    public int getHeight(){
        return this.height;
    }
    public int getArea(){
        return (this.base*this.height)/2;
    }
    @Override
    public String toString(){
        return "This triangle has a base of:"+this.base+" and a height of: "+this.height;
    }
}