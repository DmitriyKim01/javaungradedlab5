package geometry;

public class Rectangle {
    private int length;
    private int width;
    private int area;

    public Rectangle (int length, int width){
        this.length = length;
        this.width = width;
        this.area = length * width;
    }

    public int getLength(){
        return this.length;
    }


public int getWidth(){
    return this.width;
}

public int getArea(){
    return area;
}

@Override
public String toString(){
    return "The length: " + this.length + ", the width: " + this.width + ", the area: " + this.area ;
}
}
