package geometry;
import static org.junit.Assert.*;
import org.junit.Test;

public class TriangleTest {
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    @Test
    public void testToStringTriangle(){
        Triangle triangle = new Triangle(5, 2);
        assertEquals("Testing toString method", "This triangle has a base of:5 and a height of: 2" , triangle.toString());
    }

    @Test
    public void testGetBase(){
        Triangle triangle = new Triangle(5, 2);
        assertEquals("Testing getBase method", 5 , triangle.getBase());
    }

    @Test
    public void testGetHeight(){
        Triangle triangle = new Triangle(5, 2);
        assertEquals("Testing getWidth method", 2 , triangle.getHeight());
    }

    @Test
    public void testGetArea(){
        Triangle triangle = new Triangle(5, 2);
        assertEquals("Testing getWidth method", 5 , triangle.getArea());
    }

}
