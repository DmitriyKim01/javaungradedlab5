package geometry;
import static org.junit.Assert.*;
import org.junit.Test;

public class RectangleTest {
     /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    @Test
    public void testToString(){
        Rectangle rectangle = new Rectangle(5, 3);
        assertEquals("Testing toString method", "The length: 5, the width: 3, the area: 15" , rectangle.toString());
    }

    @Test
    public void testGetLength(){
        Rectangle rectangle = new Rectangle(5, 3);
        assertEquals("Testing getLength method", 5 , rectangle.getLength());
    }

    @Test
    public void testGetWidth(){
        Rectangle rectangle = new Rectangle(5, 3);
        assertEquals("Testing getWidth method", 3 , rectangle.getWidth());
    }

    @Test
    public void testGetArea(){
        Rectangle rectangle = new Rectangle(5, 3);
        assertEquals("Testing getWidth method", 15 , rectangle.getArea());
    }

}
